import { Toast } from 'solid-bootstrap';


// src/components/alert.tsx

// variants
// "primary"
// "secondary"
// "success"
// "danger"
// "warning"
// "info"
// "light"
// "dark"

const Alert: (props) => JSX.Element = (props) => {
    return (
        <Toast
            show={props.show}
            animation={true}
            class="d-inline-block m-2"
        >
            <Toast.Header bg={props.variant.toLowerCase()}>
                <img
                    src="holder.js/20x20?text=%20"
                    class="rounded me-2"
                    alt=""
                />
                <strong class="me-auto" class={props.variant === "Dark" ? "text-white" : ""}>{props.title}</strong>
                <small></small>
            </Toast.Header>
            <Toast.Body class={props.variant === "Dark" ? "text-white" : ""}>
                {props.text}
            </Toast.Body>
        </Toast>
    )
}

export default Alert;
