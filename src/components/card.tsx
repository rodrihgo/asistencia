import {
    Card,
} from "solid-bootstrap";


const CardCustom = (props) => {

    return (
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Subtitle class="mb-2 text-muted">{props.subtitle}</Card.Subtitle>
                <Card.Title>{props.title}</Card.Title>
                <Card.Text>
                    {props.text}
                </Card.Text>
                {props.children}
                {props.link && <Card.Link href={props.link}>{props.linkText}</Card.Link>}
            </Card.Body>
        </Card>
    );
}

export default CardCustom;