// src/components/modal.jsx
import { createSignal } from "solid-js";
import { Button, Modal } from "solid-bootstrap";


const CustomModal = (props) => {
    const [show, setShow] = createSignal(false);
    const handleOpen = () => setShow(true);
    const handleClose = () => {
        if (props.postClose) props.postClose();
        setShow(false);
    }
    return (
        <>
        <Button variant={props.buttonVariant} onClick={handleOpen}>
            {props.iconClass && <i class={props.iconClass}></i>}
            <span>{props.buttonText}</span>
        </Button>
        <Modal
            show={show()}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {props.title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h4>{props.description}</h4>
                {props.children}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-dark" onClick={handleClose}>Cerrar</Button>
            </Modal.Footer>
        </Modal>
        </>

    )
};

export default CustomModal;
