import { Button, Spinner } from "solid-bootstrap";


const MyButton: (props) => JSX.Element = (props) => {

    return (
        <Button
            variant={props.variant}
            type={props.type}
            size={props.size}
            disabled={props.isValid === false || props.loading}
            onClick={props.onClick ? props.onClick: () => console.log("No action defined")}
            style="width:100%;"
        >
            {props.loading === true && (<Spinner
                as="span"
                animation="grow"
                size="sm"
                role="status"
                aria-hidden="true"
            />)}
            {!props.loading && props.iconClass && <i class={props.iconClass}></i>}
            {props.text}
        </Button>
    );

}

export default MyButton;