// src/components/ProtectedRoute.jsx
import { Navigate } from "@solidjs/router";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "~/firebaseConfig";
import { createSignal, onCleanup, onMount } from "solid-js";

const ProtectedRoute = (props) => {
    const [isAuthenticated, setIsAuthenticated] = createSignal(false);
    const [loading, setLoading] = createSignal(true);

    onMount(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            debugger;
            setIsAuthenticated(!!user);
            setLoading(false);
        });

        onCleanup(() => unsubscribe());
    });

    if (loading()) {
        return <p>Loading...</p>;
    }

    return isAuthenticated() ? props.children : <Navigate href="/login" />;
};

export default ProtectedRoute;
