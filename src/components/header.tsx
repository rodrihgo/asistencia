import {Navbar, Container, Nav} from "solid-bootstrap";
import {onAuthStateChanged, signOut} from "firebase/auth";
import {useNavigate} from "@solidjs/router";
import { auth } from "~/firebaseConfig";
import {createSignal, onCleanup, onMount} from "solid-js";



const Header: (props) => JSX.Element = () => {
    const navigate = useNavigate();
    const [isAuthenticated, setIsAuthenticated] = createSignal(false);
    onMount(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            setIsAuthenticated(!!user);
        });

        onCleanup(() => unsubscribe());
    });

    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="#">
                    <img src="/images/draconiapp_short.png" height={65} />
                    </Navbar.Brand>
                <Nav class="me-auto">
                    {isAuthenticated() && (
                        <>
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/users">Usuarios</Nav.Link>
                        <Nav.Link href="/events">Eventos</Nav.Link>
                        <Nav.Link href="/logout">Logout</Nav.Link>
                        </>
                    )}
                    {!isAuthenticated() && <Nav.Link href="/login">Login</Nav.Link>}
                </Nav>
            </Container>
        </Navbar>);
}


export default Header;