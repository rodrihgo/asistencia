// src/components/AddRecord.jsx
import {createMemo, createSignal} from "solid-js";
import {collection, addDoc, getFirestore} from "firebase/firestore";
import {Container, Form, Row, Col, InputGroup, FormControl, Button } from "solid-bootstrap";
import Swal from 'sweetalert2'
import app from "~/firebaseConfig";
import MyButton from "~/components/button";


const AddRecord = ({guest, eventId = null}) => {
    const db = getFirestore(app);
    const [name, setName] = createSignal("");
    const [email, setEmail] = createSignal("");
    const [member, setMember] = createSignal(false);
    const [loading, setLoading] = createSignal(false);
    const constraints = {
        name: {
            minLength: 2,
        },
        email: {
            minLength: 5,
            match: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/
        }
    }
    const isValid = createMemo(() => (
            name().length > constraints.name.minLength &&
            email().length > constraints.email.minLength &&
            email().match(constraints.email.match)
        )
    );
    console.log({guest})
    const handleSubmit = async (event) => {
        event.preventDefault();
        setLoading(true);
        try{
            const userData = {
                nombre: name(),
                email: email(),
                invitado: guest,
                socio: Boolean(member()),
            };
            const docRef = await addDoc(collection(db, "users"), userData);
            if(docRef.id){
                if(eventId){
                    console.log(docRef);
                    const eventRef = await addDoc(collection(db, "attendances"), {
                        user: {id: docRef.id, ...userData},
                        event_id: eventId,
                        user_id: docRef.id,
                        asiste: true,
                        justifica: false,
                        fecha: new Date,
                    });
                }
                clean();
                Swal.fire({
                    icon: 'success',
                    text: 'Usuario agregado correctamente'
                })
            }
            console.log("Document written with ID: ", docRef.id);
        } catch (e) {
            console.error("Error adding document: ", e);
        }finally {
            setLoading(false);
        }
    };

    const clean = () => {
        setName("");
        setEmail("");
        setMember(false);
    }

    return (
        <Container fluid>
        <Row class="g-2">
            <Col md>
                <Form onSubmit={handleSubmit}>
                    <Form.Group class="mb-3" controlId="formBasicEmail">
                        <InputGroup>
                            <InputGroup.Text id="inputGroup-sizing-lg">Email</InputGroup.Text>
                            <FormControl type="email"
                                         aria-label="Large" aria-describedby="inputGroup-sizing-lg"
                                         aria-label="Email"
                                         required
                                         isInvalid={email() && email().length < constraints.email.minLength && !email().match(constraints.email.match)}
                                         isValid={isValid()}
                                         placeholder="Email del usuario"
                                         value={email()} onInput={(e) => setEmail(e.target.value)}
                            />
                            <Form.Control.Feedback type="invalid">
                                Por favor ingrese un email válido ej: mail@dominio.com, este debe ser mayor a {`${constraints.email.minLength}`} caracteres
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Form.Group>

                    <Form.Group class="mb-3" controlId="formBasicEmail">
                        <InputGroup>
                            <InputGroup.Text id="inputGroup-sizing-lg">Nombre</InputGroup.Text>
                            <FormControl type="text"
                                         aria-label="Large" aria-describedby="inputGroup-sizing-lg"
                                         aria-label="Nombre"
                                         placeholder="Nombre del usuario"
                                         required
                                         isInvalid={name() && name().length < constraints.name.minLength}
                                         isValid={isValid()}
                                         value={name()} onInput={(e) => setName(e.target.value)}
                            />
                            <Form.Control.Feedback type="invalid">
                                Por favor ingrese un nombre, este debe ser mayor a {`${constraints.name.minLength}`} caracteres
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Form.Group>
                    {!guest && (
                    <Form.Group class="mb-3" controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label="Es socio"
                            value={member()} onInput={(e) => setMember(e.target.checked)}
                        />
                    </Form.Group>
                    )}
                    <Form.Group class="mb-3" controlId="formBasicSubmit">
                        <MyButton
                            variant="primary"
                            type="submit"
                            size="lg"
                            iconClass="bi bi-plus"
                            text={loading() ? "Cargando...": "Crear"}
                            loading={loading()}
                            isValid={isValid()}
                        />
                    </Form.Group>
                </Form>
            </Col>
        </Row>
        </Container>
    );

};

export default AddRecord;
