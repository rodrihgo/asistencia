// src/components/AddRecord.jsx
import { createSignal, createMemo } from "solid-js";
import {collection, addDoc, getFirestore} from "firebase/firestore";
import {Container, Form, Row, Col, InputGroup, FormControl, Button, DropdownButton, Dropdown } from "solid-bootstrap";

import app from "~/firebaseConfig";
import Swal from "sweetalert2";
import MyButton from "~/components/button";


function getDatesByWeekday(startDate, targetDay, count) {
    const dates = [];
    let currentDate = new Date(startDate);

    // Ajustar la fecha inicial al primer targetDay
    while (currentDate.getDay() !== targetDay) {
        currentDate.setDate(currentDate.getDate() + 1);
    }

    // Generar la serie de fechas
    for (let i = 0; i < count; i++) {
        dates.push(new Date(currentDate));
        currentDate.setDate(currentDate.getDate() + 7);
    }

    return dates;
}



const AddRecord = ({invitado, socio}) => {
    const db = getFirestore(app);
    const [title, setTitle] = createSignal("");
    const [description, setDescription] = createSignal("");
    const [occurrency, setOccurrency] = createSignal(false);
    const [times, setTimes] = createSignal(null);
    const [day, setDay] = createSignal(null);
    const [loading, setLoading] = createSignal(false);
    const constraints = {
        title: {
            minLength: 5,
        },
        description: {
            minLength: 5,
        }
    }
    const isValid = createMemo(() => (
            title().length > constraints.title.minLength &&
            description().length > constraints.description.minLength &&
            (occurrency() ? times() > 0 : true) &&
            (occurrency() ? day() !== null : true)
        )
    );

    const days = [
        {label: "Domingo", value: 0},
        {label: "Lunes", value: 1},
        {label: "Martes", value: 2},
        {label: "Miércoles", value: 3},
        {label: "Jueves", value: 4},
        {label: "Viernes", value: 5},
        {label: "Sábado", value:6  }
    ];
    const dayDesc = {
        0: "Domingo",
        1: "Lunes",
        2: "Martes",
        3: "Miércoles",
        4: "Jueves",
        5: "Viernes",
        6: "Sábado",
    }

    // Ejemplo de uso:
    console.log({day: day()})
    console.log({loading: loading()})
    console.log({isValid: isValid()})
    const handleEvent = async (event, date: Date = new Date()) => {
        event.preventDefault();
        try {
            const docRef = await addDoc(collection(db, "events"), {
                titulo: title(),
                descripcion: description(),
                fecha: date,
            });
            console.log("Document written with ID: ", docRef.id);
            return !!docRef.id;
        } catch (e) {
            console.error("Error adding document: ", e);
        }
        return false;
    };

    const handleWeeklyEvent = async (event) => {
        event.preventDefault();
        let created = true;
        try {
            const startDate = new Date(); // Fecha actual
            const dates = getDatesByWeekday(startDate, day(), times());
            for (const date of dates) {
                const createdOne = await handleEvent(event, date);
                console.log({created, createdOne})
                created &&= createdOne;
            }
        } catch (e) {
            console.error("Error adding document: ", e);
        }
        return created;
    }

    const handleAdd = async (event) => {
        setLoading(true);
        if (occurrency() && times() > 1) {
            const created = await handleWeeklyEvent(event);
            if(created){
                Swal.fire({
                    icon: 'success',
                    text: `${times()} Eventos agregados correctamente`
                })
                clean();
                setLoading(false);
            }
            return;
        }
        const created = await handleEvent(event);
        if(created){
            clean();
            Swal.fire({
                icon: 'success',
                text: 'Evento agregado correctamente'
            })
            setLoading(false);
        }
    }

    const handleOccurency = (checked: boolean) => {
        setOccurrency(checked)
        if(!checked) {
            setDay(null);
        }
    }
    const clean = () => {
        setTitle("");
        setDescription("");
        setOccurrency(false);
        setTimes(null);
        setDay(null);
    }


    return (
        <Container fluid>
            <Row class="justify-content-md-center justify-content-sm-center">
                <Form onSubmit={handleAdd}>
                    <Form.Group class="mb-3" controlId="formBasicTitle">
                        <InputGroup>
                            <InputGroup.Text id="inputGroup-sizing-lg">Titulo</InputGroup.Text>
                            <FormControl type="text"
                                         aria-label="Large" aria-describedby="inputGroup-sizing-lg"
                                         aria-label="Titulo"
                                         required
                                         isInvalid={title() && title().length < constraints.title.minLength}
                                         isValid={isValid()}
                                         placeholder="Dale un titulo a tu evento"
                                         value={title()} onInput={(e) => setTitle(e.target.value)}
                            />
                            <Form.Control.Feedback type="invalid">
                                Por favor ingrese un titulo, minimo {`${constraints.title.minLength}`} caracteres
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Form.Group>
                    <Form.Group class="mb-3" controlId="formBasicDescription">
                        <InputGroup>
                            <InputGroup.Text>Descripcion</InputGroup.Text>
                            <FormControl as="textarea" height="250px"
                                         aria-label="Descripcion"
                                         required
                                         isInvalid={description() && description().length < constraints.description.minLength}
                                         isValid={isValid()}
                                 placeholder="Describe tu evento"
                                 value={description()} onInput={(e) => setDescription(e.target.value)}
                            />
                            <Form.Control.Feedback type="invalid">
                                Por favor ingrese un descripción completa, minimo {`${constraints.title.minLength}`} caracteres
                            </Form.Control.Feedback>
                        </InputGroup>
                    </Form.Group>
                    <Form.Group class="mb-3" controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label="Agregar recurrencia"
                                    value={occurrency()}
                                    onInput={(e) => handleOccurency(e.target.checked)}
                        />
                    </Form.Group>
                    {occurrency() && (
                        <Form.Group class="mb-3" controlId="formBasicCantidad">
                            <InputGroup class="occurrency">
                                <FormControl
                                    type="number" max={37} min={1}
                                    placeholder="Cantidad de veces"
                                    aria-label="Cantidad de veces"
                                    value={times()} onInput={(e) => setTimes(e.target.value)}
                                />
                                <DropdownButton
                                    id="dropdown-item-button"
                                    title={day() >= 0 && day() !== null ? `Todos los ${dayDesc[day()]}`:`Dia de la semana`}
                                >
                                    {days.map((d, i: number) => (
                                        <Dropdown.Item key={i} Item value={d.value} onClick={() => setDay(d.value)}>{d.label}</Dropdown.Item>
                                    ))}
                                </DropdownButton>
                            </InputGroup>
                        </Form.Group>
                    )}
                    <Form.Group class="mb-3" controlId="formBasicSubmit">
                        <MyButton
                            variant="primary"
                            type="submit"
                            size="lg"
                            iconClass="bi bi-plus"
                            text={loading() ? "Cargando...": "Crear"}
                            loading={loading()}
                            isValid={isValid()}
                        />
                    </Form.Group>
                    <style jsx dynamic>
                        {`
                          .occurrency {
                            display: flex;
                            flex-direction: row;
                            align-content: space-around;
                          }
                        `}
                    </style>
                </Form>
            </Row>
        </Container>
    );
};

export default AddRecord;
