// src/components/auth/login.jsx
import { createSignal, createMemo } from "solid-js";
import { signInWithEmailAndPassword, signOut } from "firebase/auth";
import {Container, InputGroup, Form, Row, Col, Button, FormControl } from "solid-bootstrap";
import { useNavigate } from "@solidjs/router";
import { auth } from "~/firebaseConfig";
import MyButton from "~/components/button";

const LoginForm = ()  => {

    // @hooks and initializations
    const navigate = useNavigate();
    const [email, setEmail] = createSignal("");
    const [password, setPassword] = createSignal("");
    const [user, setUser] = createSignal(null);
    const [loading, setLoading] = createSignal(false);
    const [error, setError] = createSignal(null);
    const constraints = {
        password: {
            minLength: 4,
        },
        email: {
            minLength: 5,
            match: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/
        }
    }
    const isValid = createMemo(() => (
            email().length >= constraints.email.minLength &&
            email().match(constraints.email.match) &&
            password().length >= constraints.password.minLength
        )
    );
    // @actions
    const handleLogin = async (event) => {
        event.preventDefault()
        setLoading(true);
        try {
            const userCredential = await signInWithEmailAndPassword(auth, email(), password());
            console.log(userCredential);
            setUser(userCredential.user);
            setError(null);
            navigate("/");
        } catch (e) {
            setError(e.message);
        }finally {
            setLoading(false);
        }
    };

    // @render
    return (
        <Container fluid>
            <Row class="justify-content-md-center justify-content-sm-center">
                <Col class="col-xs-12 col-sm-6 col-md-4">
                    <Form class="form" onSubmit={handleLogin}>
                        <img class="responsive" src="/images/draconiapp_newdev.png" />
                        <Form.Group class="mb-3" controlId="formBasicEmail">
                            <InputGroup>
                                <InputGroup.Text id="inputGroup-sizing-lg">Email</InputGroup.Text>
                                <FormControl type="email" placeholder="Email"
                                              required
                                              isInvalid={email().length < constraints.email.minLength && !email().match(constraints.email.match)}
                                              isValid={isValid()}
                                              value={email()}
                                              onInput={(e) => setEmail(e.target.value)}
                                />
                                <Form.Control.Feedback type="invalid">
                                    Por favor ingrese un email válido ej: mail@dominio.com, este debe ser mayor a {`${constraints.email.minLength}`} caracteres
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>

                        <Form.Group class="mb-3" controlId="formBasicPassword">
                            <InputGroup>
                                <InputGroup.Text id="inputGroup-sizing-lg">Password</InputGroup.Text>
                                <FormControl type="password" placeholder="Password"
                                          value={password()}
                                          required isInvalid={password().length < constraints.password.minLength}
                                          isValid={isValid()}
                                          onInput={(e) => setPassword(e.target.value)}
                                />
                                <Form.Control.Feedback type="invalid">
                                    Este debe ser mayor a {`${constraints.password.minLength}`} caracteres
                                </Form.Control.Feedback>
                            </InputGroup>
                        </Form.Group>
                        <Form.Group class="mb-3" controlId="formBasicSubmit">
                            <MyButton
                                variant="primary"
                                type="submit"
                                size="lg"
                                iconClass="bi bi-cursor-fill"
                                text={loading() ? "Cargando...": "Login"}
                                loading={loading()}
                                isValid={isValid()}
                            />
                            {error() && <p>{error()}</p>}
                        </Form.Group>
                    </Form>
                </Col>
            </Row>

            <style jsx dynamic>
                {`
                  .form {
                    display: flex;
                    flex-direction: column;
                    align-content: center;
                  }
                `}
            </style>
        </Container>
    );
};


export default LoginForm;