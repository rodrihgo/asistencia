// src/components/ListEvents.jsx
import { createSignal, onMount } from "solid-js";
import { useNavigate } from "@solidjs/router";
import {collection, getFirestore, query, orderBy, getDocs, where} from "firebase/firestore";
import app from "~/firebaseConfig";
import {Badge, Button, Container, ListGroup} from "solid-bootstrap";
import FormEvent from "~/components/add-event";
import CustomModal from "~/components/modal";

const ListEvents = () => {
    const navigate = useNavigate();
    const [records, setRecords] = createSignal([]);

    const fetchRecords = async () => {
        const db = getFirestore(app);
        const date = new Date();
        date.setDate(date.getDate() - 90);
        const condition = where("fecha", ">=", date);
        const order = orderBy("fecha", "desc");
        const q = query(collection(db, "events"), condition, order);
        const querySnapshot = await getDocs(q);
        const docs = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        console.log({docs})
        setRecords(docs);
    };

    const handleClick = (id: number) => {
        navigate(`/event/${id}`)
    }
    const handleBack = () => {
        window.history.back();
    }

    const formatDate = (date) => {
        return new Date(date.toDate()).toLocaleString({dateStyle: 'short', timeStyle: 'short'});
    }

    onMount(() => {
        fetchRecords();
    });

    return (
        <Container>
            <Button onClick={handleBack}><i class="bi bi-arrow-90deg-left"></i>Volver</Button>
            <h1>Últimos Eventos</h1>
            <ListGroup>
                {records().map((record) => (
                    <ListGroup.Item key={record.id} onClick={() => handleClick(record.id)}>
                        <div class="event-item">
                            <span>{record.titulo}</span>
                            <span class="small">
                                {formatDate(record.fecha)}
                            </span>
                        </div>
                    </ListGroup.Item>
                ))}
            </ListGroup>
            <CustomModal
                buttonVariant="success"
                iconClass="bi bi-plus"
                title="Agregar Evento"
                buttonText="Agregar"
                postClose={fetchRecords}
            >
                <FormEvent />
            </CustomModal>
            <style jsx dynamic>
                {`
                  .event-item {
                    display: flex;
                    flex-direction: column;
                    align-content: space-between;
                  }
                  .event-item:hover{
                    background-color: #f8f9fa;
                  }
                  .small {
                    font-size: 0.8em;
                  }
                `}
            </style>
        </Container>
    );
};

export default ListEvents;
