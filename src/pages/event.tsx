// src/components/ListEvents.jsx
import { createSignal, onMount } from "solid-js";
import { useNavigate, useParams } from "@solidjs/router";
import {
    ButtonGroup,
    Button,
    Container,
    ListGroup,
    Badge,
    OverlayTrigger,
    Tooltip,
    Table,
} from "solid-bootstrap";
import Swal from 'sweetalert2'
import {collection, getFirestore, getDocs, where, doc, getDoc, addDoc, query, updateDoc} from "firebase/firestore";
import app from "~/firebaseConfig";
import FormUser from "~/components/add-user";
import MyButton from "~/components/button";
import CustomModal from "~/components/modal";

const ListEvents = () => {
    const navigate = useNavigate();
    const params = useParams();
    const [eventoId, setEventoId] = createSignal(null);
    const [users, setUsers] = createSignal(null);
    const [evento, setEvento] = createSignal(null);
    const [loading, setLoading] = createSignal(false);
    const [records, setRecords] = createSignal([]);

    const fetchUsers = async () => {
        const db = getFirestore(app);
        const querySnapshot = await getDocs(collection(db, "users"));
        const docs = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        setUsers(docs);
    };

    const fetchRecords = async () => {
        const db = getFirestore(app);
        const q = query(collection(db, "attendances"), where("event_id", "==", params.id));
        const querySnapshot = await getDocs(q);
        const docs = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        setRecords(docs);
    };

    const getEvent = async (id: number) => {
        try {
            console.log({id})
            const db = getFirestore(app);
            const docRef = doc(db, "events", id);
            const docSnap = await getDoc(docRef);
            if (docSnap.exists()) {
                const eventData = {
                    id,
                    ...docSnap.data()
                };
                setEvento(eventData);
                console.log("Document data:", eventData);
            } else {
                console.log("No such document!");
                setEvento(null);
            }
        } catch (e) {
            console.error("Error getting document: ", e);
            setEvento(null);
        }
    }


    const handleAttend = async (user: object, event: object, asiste: boolean | null, justifica: boolean) => {
        try {
            const db = getFirestore(app);
            const docRef = await addDoc(collection(db, "attendances"), {
                event_id: params.id,
                user_id: user.id,
                user: user,
                event: event,
                asiste,
                justifica,
                fecha: new Date,
            });
            console.log("Document written with ID: ", docRef.id);
        } catch (e) {
            console.error("Error adding document: ", e);
        }
    }

    const fixAttendances = async () => {
        for(const record of records()) {
            await fixAttendance(record.id);
        }
        Swal.fire({type: 'info', title: 'EVENTO', text: 'Corregido!'})
    }

    const fixAttendance = async (id: string) => {
        const db = getFirestore(app);
        const docRef = await doc(db, "attendances", id);
        await updateDoc(docRef, {
            event: evento()
        });
    }
    const handleUpdAttend = async (id: string, asiste: boolean, justifica: boolean, setload: boolean) => {
        try {
            if(setload) setLoading(true);
            const db = getFirestore(app);
            const docRef = await doc(db, "attendances", id);
            await updateDoc(docRef, {
                asiste,
                justifica,
                modificado: new Date,
            });
            await fetchRecords();
            console.log("Document updated with ID: ", docRef.id);
        } catch (e) {
            console.error("Error adding document: ", e);
        }finally {
            if(setload) setLoading(false);
        }
    }

    const handleBack = () => {
        window.history.back();
    }


    onMount(async () => {
        setEventoId(params.id);
        await getEvent(params.id);
        await fetchRecords();
        await fetchUsers();
    });

    const handleNewAttend = async (user: object, event: object, asiste: boolean, justifica: boolean) => {
        setLoading(true);
        await handleAttend(user, event, asiste, justifica);
        await fetchRecords();
        setLoading(false);
    }

    const handleAttendAll = async () => {
        setLoading(true);
        for(const record of records()) {
            await handleUpdAttend(record.id, true, false, false);
        }
        await fetchRecords();
        setLoading(false);
    }
    const handleAddMembers = async () => {
        // await fetchUsers();
        setLoading(true);
        console.log(users());
        for(const user of users()) {
            console.log(user);
            if (!user.socio && !user.activo){
                continue;
            }
            await handleAttend(user, evento(), null, false);
        }
        await fetchRecords();
        setLoading(false);
    }
    const renderEvent = () => {
        if (!evento()) {
            return <h3>Cargando...</h3>
        }
        return <h3>{evento().descripcion}</h3>
    };
    return (
        <Container>
            <Button type="button" onClick={handleBack}><i class="bi bi-arrow-90deg-left"></i>Volver</Button>
            <h1>{evento() && evento().titulo}</h1>
            <p style="text-align: justify">
                {renderEvent()}
            </p>
            <span>
                {evento() && new Date(evento().fecha.toDate()).toLocaleString()}
                {false && <MyButton onClick={fixAttendances} text="fix attendances" />}
            </span>
            <Table class="mt-2" striped bordered hover>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Usuario</th>
                        <th>Correo</th>
                        <th>Asiste</th>
                        <th>Se excusa</th>
                        <th>Acciones</th>
                    </tr>

                </thead>
                <tbody>
                {records().map((record) => (
                    <tr class="w-16 md:w-32 lg:w-48" key={record.id}>

                        <td>{record.id }</td>
                        <td>{record.user.nombre ?? 'NN'}</td>
                        <td>{record.user.email ?? 'NE'}</td>
                        <td>
                            <Badge pill bg={record.asiste ? `success`: (record.justifica || record.asiste === null ? `info`: `danger`)}>{record.asiste ? 'Si': (record.asiste === null )? 'N/A': 'No'}</Badge>
                        </td>
                        <td>
                            {record.asiste == false ? `${record.justifica ? 'Si': 'No'}`: 'N/A'}

                        </td>
                        <td>
                            <ButtonGroup>
                                <MyButton
                                    variant={`danger`}
                                    onClick={() => handleUpdAttend(record.id, false, false, true) }
                                    iconClass={"bi bi-trash"}
                                    loading={loading()}
                                    isValid={true}
                                    text={`No Asiste`}
                                />
                                <MyButton
                                    variant={`success`}
                                    onClick={() => handleUpdAttend(record.id, true, false, true) }
                                    iconClass={"bi bi-check-all"}
                                    loading={loading()}
                                    isValid={true}
                                    text={'Asiste'}
                                />
                                <MyButton
                                    variant="warning"
                                    onClick={() => handleUpdAttend(record.id, false, true, true) }
                                    iconClass="bi bi-check"
                                    loading={loading()}
                                    isValid={true}
                                    text={`No Puede`}
                                />
                            </ButtonGroup>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
            <ButtonGroup>
            {records().length && <MyButton
                type="button"
                onClick={() => handleAttendAll()}
                text="Asisten todos"
                loading={loading()}
                isValid={true}
            />}
            {!records().length && <MyButton
                type="button"
                onClick={() => handleAddMembers()}
                text="Añadir socios"
                loading={loading()}
                isValid={true}
            />}
            </ButtonGroup>

            <h1>Otros usuarios </h1>
            <ListGroup class="mt-2">
                {users() && users().filter((record) => !record.socio && record.activo).map((user) => (
                    <ListGroup.Item class="user w-16 md:w-32 lg:w-48" key={user.id} >
                        <div class="user-item">
                            { user.nombre ?? 'NN'}
                        </div>
                        <div class="user-item">
                           {user.email ?? 'NE'}
                        </div>
                        <div class="user-item">
                        <OverlayTrigger
                            placement="right"
                            delay={{ show: 250, hide: 1000 }}
                            overlay={<Tooltip id="button-tooltip">Añadirlo al evento</Tooltip>}
                        >
                        <MyButton
                            variant={"success"}
                            onClick={() => handleNewAttend(user, evento(), true, false) }
                            iconClass={"bi bi-plus"}
                            loading={loading()}
                            isValid={true}
                        />
                        </OverlayTrigger>
                        </div>
                    </ListGroup.Item>
                ))}
            </ListGroup>
            <ButtonGroup>
            <CustomModal
                buttonVariant="success"
                iconClass="bi bi-plus"
                title="Agregar Usuario a Evento"
                buttonText="Agregar usuario"
                postClose={fetchRecords}
            >
                <FormUser guest={false} eventId={eventoId()} />
            </CustomModal>
            <CustomModal
                buttonVariant="primary"
                iconClass="bi bi-plus"
                title="Agregar Invitado a Evento"
                buttonText="Agregar Invitado"
                postClose={fetchRecords}
            >
                <FormUser guest={true} eventId={eventoId()} />
            </CustomModal>
            </ButtonGroup>
            <style jsx dynamic>
                {`
                  .user{
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    flex-direction: row;
                    gap: 1em;
                  }
                  .user-item {
                    display: flex;
                    flex-direction: column;
                    align-content: space-between;
                  }
                  .user-item:hover{
                    background-color: #f8f9fa;
                  }
                  .small {
                    font-size: 0.8em;
                  }
                `}
            </style>
        </Container>
    );
};

export default ListEvents;
