// src/components/ListEvents.jsx
import { createSignal, onMount } from "solid-js";
import { useNavigate } from "@solidjs/router";
import {
    Container,
    Form,
    ButtonGroup,
    Button,
    Row,
    Col,
    CardGroup,
} from "solid-bootstrap";
import Swal from 'sweetalert2'
import {collection, getFirestore, getCountFromServer, query, where, getDocs, Timestamp, orderBy} from "firebase/firestore";
import app from "~/firebaseConfig";
import CardCustom from "~/components/card";
import Chart from 'chart.js/auto'
import {create} from "domain";

const Dashboard = () => {
    const navigate = useNavigate();
    const today = new Date();
    const MONTHS_AGO = 3;
    const currentMonth = `${today.getFullYear()}-${today.getMonth() + 1}`;
    const getLastDayOfMonth = (dateYearMonth: string) =>{ // string format: 'year-month'
        const auxiliaryToDate = new Date(`${dateYearMonth}-01`);
        auxiliaryToDate.setMonth(auxiliaryToDate.getMonth() + 1);
        return new Date(auxiliaryToDate.setDate(auxiliaryToDate.getDate() - 1));
    }
    const startDate = new Date(today.setMonth(today.getMonth() - (MONTHS_AGO- 1)));
    const endDate = getLastDayOfMonth(currentMonth);
    // Signals
    const [attendances, setAttendance] = createSignal<number>(0);
    const [attendanceReport, setAttendanceReport] = createSignal<any[]>([]);
    const [members, setMembers] = createSignal<number>(0);
    const [guests, setGuests] = createSignal<number>(0);
    const fromStr = `${startDate.getFullYear()}-${startDate.getMonth() + 1}`;
    const toStr = `${endDate.getFullYear()}-${endDate.getMonth() + 1}`;
    console.log({fromStr, toStr})
    const [fromDate, setFromDate] = createSignal<number | undefined>(fromStr);
    const [toDate, setToDate] = createSignal<string | undefined>(toStr);
    console.log(fromDate(), toDate());
    const [chartOne, setChartOne] = createSignal<Chart>(undefined);
    const [chartTwo, setChartTwo] = createSignal<Chart>(undefined);
    const [chartThree, setChartThree] = createSignal<Chart>(undefined);


    const countAttendancesRecords = async () => {
        const fromD = new Date(`${fromDate()}-01`);
        const toD = getLastDayOfMonth(toDate());
        const docs = await getAttendancesRecords(fromD, toD)
        if(docs.length === 0) {
            setAttendance(0);
            return;
        }
        let att = 0;
        for (const d of docs) {
            att += d.asiste ? 1 : 0;
        }
        setAttendance(Math.round(100 * att / docs.length), 2);
    };

    const getAttendancesRecords = async (fromDate: Date, toDate: Date) => {
        const db = getFirestore(app);
        const attendanceRef = collection(db, "attendances");
        const startTimestamp = Timestamp.fromDate(fromDate);
        const endTimestamp = Timestamp.fromDate(toDate);
        console.log({startTimestamp, endTimestamp})
        const order = orderBy("fecha", "asc");
        const q =  await query(
                    attendanceRef ,
                    where("fecha", ">=", startTimestamp),
                    where("fecha", "<=", endTimestamp), order);
        const querySnapshot = await getDocs(q);
        const docs = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        setAttendanceReport(docs);
        return Promise.resolve(docs);
    };

    const countMembersRecords = async () => {
        const db = getFirestore(app);
        const q = query(collection(db, "users"), where("socio", "==", true));
        const querySnapshot = await getCountFromServer(q);
        const total = querySnapshot.data().count;
        console.log({total})
        setMembers(total);
    };

    const countGuestsRecords = async () => {
        const db = getFirestore(app);
        const q = query(collection(db, "users"), where("socio", "==", false));
        const querySnapshot = await getCountFromServer(q);
        const total = querySnapshot.data().count;
        console.log({total})
        setGuests(total);
    };
    const handleAttendances = () => {
        navigate("/events");
    }
    const handleUsers = (id: number) => {
        navigate("/users")
    }
    const defineLabel = (fromDate: string, toDate: string) => { // string format: 'year-month'
        const fromD = new Date(`${fromDate}-01`);
        const currentYear = new Date().getFullYear();
        const labels = [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre',
        ];
        const formatLabel = (current: Date) => {
            return `${labels[current.getMonth()]} ${current.getFullYear()}`
        }
        if(!fromDate || !toDate) return [`${labels[0]} ${currentYear}`];
        const currentDate = getLastDayOfMonth(toDate);
        let finalLabels = [formatLabel(currentDate)];
        let yearMonths = [{year: currentDate.getFullYear(), month: currentDate.getMonth()}];
        currentDate.setMonth(currentDate.getMonth() - 1);
        while(currentDate > fromD){
            finalLabels =[formatLabel(currentDate), ...finalLabels];
            yearMonths = [{year: currentDate.getFullYear(), month: currentDate.getMonth()}, ...yearMonths];
            currentDate.setMonth(currentDate.getMonth() - 1);
        }
        return [finalLabels, yearMonths];

    }

    const createChartCanvas = (id: number) => {
        const dom = document.createElement('canvas', {id: `chart-${id}`});
        document.getElementById(`chart-${id}_container`).appendChild(dom);
        return dom;
    }
    const removeChartCanvas = (id: number) => {
        const elem = document.getElementById(`chart-${id}_container`);
        if(!elem) return;
        while (elem.lastElementChild) {
            elem.removeChild(elem.lastElementChild);
        }
    }

    const captureData = (rawData, yearMonths) => {
        let data = [];
        for (const ym of yearMonths) {
            // if(!data.hasOwnProperty(`${ym.year}-${ym.month}`)){
            let acum = 0;
            // }
            for (const d of rawData){
                const date = d.fecha.toDate();
                if(date.getFullYear() === ym.year && date.getMonth() === ym.month){
                    acum += d.asiste ? 1 : 0;
                }
            }
            data = [...data, acum]
        }
        return data;
    }
    const renderChart = (rawData) => {

        const [labels, yearMonths] = defineLabel(fromDate(), toDate())
        const data = {
            labels,
            datasets: [{
                label: 'Evolución de asistencias',
                data: captureData(rawData, yearMonths),
                fill: true,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            }]
        };
        removeChartCanvas(1);
        removeChartCanvas(2);
        removeChartCanvas(3);
        const domOne = createChartCanvas(1)
        const domTwo = createChartCanvas(2)
        const domThree = createChartCanvas(3)
        const one = new Chart(domOne, { type: 'bar', data});
        const two = new Chart(domTwo, { type: 'line', data});
        const three = new Chart(domThree, { type: 'pie', data});
        setChartOne(one);
        setChartTwo(two);
        setChartThree(three);
    }

    const convertDate = (date: string) => {

    }
    const onFetch = async() => {

        const fromD = new Date(`${fromDate()}-01`);
        const auxiliaryToDate = new Date(`${toDate()}-01`);
        const toD = new Date(auxiliaryToDate.setMonth(auxiliaryToDate.getMonth() + 1));
        if (fromD > toD) {
            Swal.fire({type: 'error', title: 'ERROR', text: 'La fecha de inicio no puede ser mayor a la fecha final'});
            return;
        }
        const attRecords = await getAttendancesRecords(fromD, toD);
        renderChart(attRecords);

        // Swal.fire({type: 'info', title: 'INFORMACION', text: 'Proximamente ;)!'})
    }
    const onDownload = () => {
        const header = ['Evento ID', 'Fecha', 'Miembro', 'Tipo', 'Evento', 'Asistencia']
        const data = attendanceReport().map((r) => {
                return [
                    `${r.event_id}`,
                    `${r.fecha.toDate().toLocaleDateString()}`,
                    `${r.user.nombre}`,
                    `${r.user.socio ? 'Socio' : 'Invitado'}`,
                    `${r.event.titulo}`,
                    `${r.asiste ? 'Asistió' : 'No asistió'}`
                ].join('\t')
            }
        ).join('\n');
        console.log({header,data})
        navigator.clipboard.writeText(header.join('\t') +'\n'+ data);// copy to clipboard
        Swal.fire({type: 'info', title: 'INFORMACION', text: 'Copiado al portapapeles ;)!'})

    }
    onMount(async () => {
        await countAttendancesRecords();
        countMembersRecords();
        countGuestsRecords();
        renderChart(attendanceReport());
    });


    return (
        <div>
            <h1>Dashboard</h1>
            <Container>
                <CardGroup>
                    <CardCustom
                        title={`${attendances()}%`}
                        subtitle={`Asistencia últimos ${MONTHS_AGO} meses`}
                        link={`/events`}
                        linkText={`Ver mas`}
                    />

                    <CardCustom
                        title={`${members()}`}
                        subtitle={`Socios`}
                        link={`/users`}
                        linkText={`Ver mas`}
                    />
                    <CardCustom
                        title={`${guests()}`}
                        subtitle={`Invitados`}
                    />

                </CardGroup>
                <Container fluid>
                    <Form>
                        <Row class="reporte">
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicDesde">
                                    <Form.Label>Desde</Form.Label>
                                    <Form.Control type="month" placeholder="Desde"
                                        value={fromDate()}
                                        onChange={(e) => setFromDate(e.target.value)}
                                    />
                                </Form.Group>

                            </Col>
                            <Col>
                                <Form.Group className="mb-3" controlId="formBasicHasta">
                                    <Form.Label>Hasta</Form.Label>
                                    <Form.Control type="month" placeholder="Hasta"
                                      value={toDate()}
                                      onChange={(e) => setToDate(e.target.value)}
                                    />
                                </Form.Group>
                            </Col>
                            <Col>
                                <ButtonGroup>
                                <Button
                                    disabled={!fromDate() || !toDate()}
                                    variant="success"
                                    onClick={onFetch}>Aplicar</Button>
                                <Button
                                    disabled={!fromDate() || !toDate()}
                                    onClick={onDownload}>Copiar reporte</Button>
                                </ButtonGroup>
                            </Col>
                        </Row>
                    </Form>
                </Container>
            <CardGroup>
                <CardCustom
                    title={``}
                    subtitle={`% por meses`}
                >
                    <div id="chart-1_container"></div>
                </CardCustom>
                <CardCustom
                    title={``}
                    subtitle={`% por meses`}
                >
                    <div id="chart-2_container"></div>
                </CardCustom>
                <CardCustom
                    title={``}
                    subtitle={`% por meses`}
                >
                    <div id="chart-3_container"></div>
                </CardCustom>
            </CardGroup>

            </Container>
            <style jsx dynamic>
                {`
                  .reporte {
                    display: flex;
                    flex-direction: row;
                    align-items: center;
                    align-content: center;
                    justify-content: center;
                  }
                  .small {
                    font-size: 0.8em;
                  }
                `}
            </style>
        </div>
    );
};

export default Dashboard;
