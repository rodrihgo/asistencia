// src/components/ListEvents.jsx
import { createSignal, onMount } from "solid-js";
import { useNavigate } from "@solidjs/router";
import { collection, getFirestore, query, where, getDocs } from "firebase/firestore";
import {Badge, Button, ListGroup} from "solid-bootstrap";
import CustomModal from "~/components/modal";
import FormUser from "~/components/add-user";
import app from "~/firebaseConfig";

const ListUsers = () => {
    const navigate = useNavigate();
    const [records, setRecords] = createSignal([]);

    const fetchRecords = async () => {
        const db = getFirestore(app);
        const q = query(collection(db, "users"), where("invitado", "==", false));
        const querySnapshot = await getDocs(q);
        const docs = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        setRecords(docs);
    };

    const handleClick = (id) => {
        navigate(`/user/${id}`);
    }
    const handleBack = () => {
        window.history.back();
    }
    onMount(() => {
        fetchRecords();
    });

    return (
        <>
            <Button onClick={handleBack}><i class="bi bi-arrow-90deg-left"></i>Volver</Button>
            <h1>Usuarios</h1>
            <ListGroup>
                {records().map((record) => (
                    <ListGroup.Item key={record.id} onClick={() => handleClick(record.id)}>
                        <div class="socio-item">
                            <span>
                                {record.nombre}
                                {record.socio && <Badge pill bg={`success`}>Socio</Badge>}
                            </span>
                            <span class="small">{record.email}

                            </span>
                        </div>
                    </ListGroup.Item>
                ))}
            </ListGroup>
            <CustomModal
                buttonVariant="success"
                iconClass="bi bi-plus"
                title="Agregar Usuario"
                buttonText="Agregar"
                postClose={fetchRecords}
            >
                <FormUser guest={false} />
            </CustomModal>
            <style jsx dynamic>
                {`
                  .socio-item {
                    display: flex;
                    flex-direction: column;
                    align-content: space-between;
                  }
                  .socio-item:hover{
                    background-color: #f8f9fa;
                  }
                  .small {
                    font-size: 0.8em;
                  }
                `}
            </style>
        </>
    );
};

export default ListUsers;
