// src/pages/logout.jsx
import { onMount } from "solid-js";
import { signOut } from "firebase/auth";
import { useNavigate } from "@solidjs/router";
import { auth } from "~/firebaseConfig";

export default function() {
    // @hooks and initializations
    const navigate = useNavigate();

    // @actions
    const handleLogout = async () => {
        await signOut(auth);
    };

    // @life-cycle
    onMount(async () => {
        await handleLogout();
        navigate("/login");

    });

    // @render
    return (
        <div>
            Logout page :)
        </div>
    );
};
