// src/components/ListEvents.jsx
import { createSignal, onMount } from "solid-js";
import { useNavigate, useParams } from "@solidjs/router";
import {
    ButtonGroup,
    Button,
    Container,
    ListGroup,
    Badge,
    Tooltip,
    Table, CardGroup,
} from "solid-bootstrap";
import {collection, getFirestore, getDocs, where, doc, getDoc, query } from "firebase/firestore";
import app from "~/firebaseConfig";
import FormUser from "~/components/add-user";
import CustomModal from "~/components/modal";
import CardCustom from "~/components/card";

const ListEvents = () => {
    const navigate = useNavigate();
    const params = useParams();
    const [userId, setEventoId] = createSignal(null);
    const [user, setUser] = createSignal(null);
    const [records, setRecords] = createSignal([]);
    const [attendances, setAttendances] = createSignal(0);
    const [excuses, setExcuses] = createSignal(0);
    const [totalize, setTotalize] = createSignal(0);
    const [unattendances, setUnattendances] = createSignal(0);



    const fetchRecords = async () => {
        const db = getFirestore(app);
        const q = query(collection(db, "attendances"), where("user_id", "==", params.id));
        const querySnapshot = await getDocs(q);
        let asiste = 0;
        let justifica = 0;
        let inasiste = 0;
        let total = 0;
        const docs = querySnapshot.docs.map((doc) => {
            asiste += doc.data().asiste ? 1 : 0;
            justifica += doc.data().justifica ? 1 : 0;
            inasiste += doc.data().asiste === false ? 1 : 0;
            total++;
            return ({
                id: doc.id,
                ...doc.data(),
            })
        });
        setAttendances(asiste);
        setExcuses(justifica);
        setUnattendances(inasiste);
        setTotalize(total);
        setRecords(docs);
    };

    const getUser = async (id: number) => {
        try {
            console.log({id})
            const db = getFirestore(app);
            const docRef = doc(db, "users", id);
            const docSnap = await getDoc(docRef);
            if (docSnap.exists()) {
                setUser(docSnap.data());
                console.log("Document data:", docSnap.data());
            } else {
                console.log("No such document!");
                setUser(null);
            }
        } catch (e) {
            console.error("Error getting document: ", e);
            setUser(null);
        }
    }




    const handleBack = () => {
        window.history.back();
    }


    onMount(async () => {
        setEventoId(params.id);
        await getUser(params.id);
        await fetchRecords();
    });

    return (
        <Container>
            <Button type="button" onClick={handleBack}><i class="bi bi-arrow-90deg-left"></i>Volver</Button>
            <h1>{user() && user.nombre} {user.socio && <Badge pill bg={`success`}>Socio</Badge>}</h1>
            <span>

                {user() && user.email}
            </span>
            <Container>
                <CardGroup>
                    <CardCustom
                        title={`${attendances()} / ${totalize()}`}
                        subtitle={`Asistencias`}
                    />

                    <CardCustom
                        title={`${unattendances()} / ${totalize()}`}
                        subtitle={`Inasistencias`}
                    />
                    <CardCustom
                        title={`${excuses()} / ${totalize()}`}
                        subtitle={`Excusado`}
                    />

                </CardGroup>
            </Container>
            <Table class="mt-2" striped bordered hover>
                <thead>
                    <tr>
                        <th>Reunion</th>
                        <th>Asiste</th>
                        <th>Se excusa</th>
                    </tr>

                </thead>
                <tbody>
                {records().map((record) => (
                    <tr class="w-16 md:w-32 lg:w-48" key={record.id}>
                        <td><a href={`/event/${record.event_id}`}>{record.event_id}</a></td>
                        <td>
                            <Badge pill bg={record.asiste ? `success`: (record.justifica || record.asiste === null ? `info`: `danger`)}>{record.asiste ? 'Si': (record.asiste === null )? 'N/A': 'No'}</Badge>
                        </td>
                        <td>
                            {record.asiste == false ? `${record.justifica ? 'Si': 'No'}`: 'N/A'}

                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </Container>
    );
};

export default ListEvents;
