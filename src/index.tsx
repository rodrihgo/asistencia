/* @refresh reload */
import { render } from 'solid-js/web';
import { Router, Route } from "@solidjs/router";
// import '~/index.css';
// import '~/app.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import App from '~/App';
// @pages
import Dashboard from '~/pages/dashboard';
import User from '~/pages/user';
import Users from '~/pages/users';
import Events from '~/pages/events';
import Event from '~/pages/event';
import NotFound from '~/pages/notfound';
// import ProtectedRoute from "~/components/protected";
import LoginPage from '~/pages/login';
import LogoutPage from "~/pages/logout";

const root = document.getElementById('root');

if (import.meta.env.DEV && !(root instanceof HTMLElement)) {
  throw new Error(
    'Root element not found. Did you forget to add it to your index.html? Or maybe the id attribute got misspelled?',
  );
}

render(() => (
    <Router root={App}>
        <Route path="/login" component={LoginPage} />
        <Route path="/logout" component={LogoutPage} />
        <Route path="/users" component={Users} />
        <Route path="/events" component={Events} />
        <Route path="/event/:id" component={Event} />
        <Route path="/user/:id" component={User} />
        <Route path="/" component={Dashboard} />
        <Route path="*" component={NotFound} />
    </Router>
), root!);
