import type { Component } from 'solid-js';
import {onAuthStateChanged, signOut} from "firebase/auth";


import logo from './logo.svg';
import styles from './App.module.css';
// import './index.css'
import { useNavigate } from "@solidjs/router";
import {createSignal, onCleanup, onMount} from "solid-js";
import { Container } from "solid-bootstrap";
import Header from "~/components/header";
// import ProtectedRoute from "~/components/protected";

const App: (props) => JSX.Element = (props) => {



  return (
    <div class={styles.App}>
        <Header />
        <Container>
        {props.children}
        </Container>
    </div>

  );
};

export default App;
